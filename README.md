# Co-operative Technologists Ansible Playbooks

These Playbooks are designed to be used on Debian Stretch virtual servers.

## live2dev

Ansible playbook to update the [dev site](https://dev.coops.tech/) from the [live site](https://www.coops.tech/).

```bash
export SERVERNAME="webarch1.co.uk"
ansible-playbook live2dev.yml -i "${SERVERNAME}," -e "hostname=${SERVERNAME}"
``` 

See also [the wiki documentation](https://wiki.coops.tech/wiki/CoTech_WordPress) and the [GitHub project](https://github.com/cotech/website).

## Discourse Install

The Discourse code has been moved to the [Webarchitects Discourse repo](https://git.coop/webarch/discourse) and the notes to the [CoTech wiki](https://wiki.coops.tech/wiki/Community_%28Discourse_site%29).
